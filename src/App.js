import React, { useEffect } from "react";
import { db } from "./firebase";
import { useSelector, useDispatch } from "react-redux";
import { getProductsThunk, selectProducts, selectPage, selectModal } from "./redux/eCommerceSlice";
import Header from "./components/Header/Header";
import styles from "./App.module.css";
import Showcase from "./components/Showcase/Showcase";
import Cart from "./components/Cart/Cart";
import "bootstrap/dist/css/bootstrap.min.css";
import { renderSwitch } from "./utils/renderSwitch";
import Modal from "./components/Modal/Modal";

const App = () => {
	const dispatch = useDispatch();
	const storeProducts = useSelector(selectProducts);
	const page = useSelector(selectPage);
	const modal = useSelector(selectModal);

	useEffect(() => {
		if (modal) {
			document.body.style.overflow = "hidden";
		} else {
			document.body.style.overflow = "unset";
		}
	}, [modal]);

	// probably i should clear storeProducts after order submit
	if (Object.keys(storeProducts).length < 1) {
		dispatch(getProductsThunk(db, "products"));
	}

	return (
		<div className="App">
			<Header page={page} />
			<main className={styles.container}>{renderSwitch(page, <Showcase />, <Cart />)}</main>
			{modal && <Modal data={modal} />}
		</div>
	);
};

export default App;

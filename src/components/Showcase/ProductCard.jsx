import React from 'react';
import { Badge, Button, Card } from 'react-bootstrap';
import style from "./ProductCard.module.css";
import { useDispatch } from 'react-redux';
import { increaseCartProduct, decreaseCartProduct, openModal } from './../../redux/eCommerceSlice';


const ProductCard = ({ product }) => {
  const { title, price, description, code, quantity } = product;
  const dispatch = useDispatch();
  const plus = () => dispatch(increaseCartProduct(code));
  const minus = () => dispatch(decreaseCartProduct(code));

  const height = Math.floor(window.innerHeight * 0.6);
  const width = Math.floor(window.innerWidth * 0.6);
  const modalData = {
    type: "image", 
    body: `https://loremflickr.com/${width}/${height}?random=${code}`
  };
  const showModal = () => dispatch(openModal(modalData));

  return (
    <Card style={{ width: '18rem' }}>
      <div className={style.relative} onClick={showModal} >
        <span className={style.counter}><Badge variant="warning">{quantity}</Badge></span>
        <Card.Img variant="top" src={`https://loremflickr.com/320/240?random=${code}`} />
      </div>
      <Card.Header>
        <div className={style.flex}>
          <div className={`${style.flex1} ${style.sizeMd}`}><Badge variant="secondary" >{price} $</Badge></div>
          {quantity && <Button variant="outline-info" onClick={minus} className={style.marginSm}>-</Button>}
          <Button variant="outline-info" onClick={plus}>+</Button>
        </div>
      </Card.Header>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Text>Code: {code}</Card.Text>
      </Card.Body>
    </Card>
  )
}

export default ProductCard

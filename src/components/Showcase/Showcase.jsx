import React from 'react';
import Container from 'react-bootstrap/Container';
import ProductCard from './ProductCard';
import style from "./Showcase.module.css";
import { useSelector } from 'react-redux';
import { selectCart, selectProducts } from './../../redux/eCommerceSlice';

const Showcase = () => {
  const storeProducts = useSelector(selectProducts);
  const storeCart = useSelector(selectCart);

  const mappedCards = Object.keys(storeProducts)
    .map(code => <ProductCard key={code} product={{ ...storeProducts[code], quantity: storeCart[code] }} />);
  
  return (
    <Container fluid="xl" className={style.grid}>
      {mappedCards}
    </Container>
  )
}

export default Showcase

import React from "react";
import { Image } from "react-bootstrap";
import style from "./Modal.module.css";
import { useDispatch } from "react-redux";
import { closeModal, setPage } from "../../redux/eCommerceSlice";

const Modal = ({ data }) => {
	const dispatch = useDispatch();
  const { type, body, onClose } = data;
  const close = () => {
    dispatch(closeModal());
    if (onClose) {
      dispatch(setPage(onClose));
    }
  }

  return (
    <div className={style.wrapper} onClick={close}>
      <div className={style.container}>
        {/* <div className={style.header}>
          <Button variant="outline-info" onClick={close}>close</Button>
        </div> */}
        <div className={style.body}>
          {type === "image" && <Image src={body} rounded />}
          {type === "text" && <p>{body}</p>}
        </div>
      </div>
    </div>
  )
}

export default Modal; 

import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { openModal, selectCart, selectProducts, setCart, setProducts } from '../../../redux/eCommerceSlice';
import { getCartBody, getCartCounter, getCartTotal } from '../../../redux/storeGetters';
import date from '../../../utils/date';
import Input from './Input';
import { db } from "./../../../firebase";
import { setFirebaseCollection } from '../../../api/api';
import { variables } from '../../../utils/variables';

const CartForm = () => {
  const dispatch = useDispatch();

  const storeProducts = useSelector(selectProducts);
  const storeCart = useSelector(selectCart);
  const total = getCartTotal(storeProducts, storeCart);
  const counter = getCartCounter(storeCart);
  
  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    if (form.checkValidity() === false) {
      event.stopPropagation();
    } else {
      setValidated(true);
      const customer = Array.prototype.reduce
        .call(event.target, (acc, curr) => curr.id ? { ...acc, [curr.id]: curr.value } : acc, {});
      const products = getCartBody(storeProducts, storeCart);
      const time = date();
      const order = { customer, products, total, time };
      // dispatch(setOrder(order));
      setFirebaseCollection(db, "orders", order);
      dispatch(setCart({}));
      dispatch(setProducts({}));
      const modalBody = {
        type: "text",
        body: "Thanks for order, dude! You make me rich :)",
        onClose: variables.SHOWCASE
      };
      dispatch(openModal(modalBody));
    }
    setValidated(true);
  };

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Form.Row>
        <Input title="first name" disabled={!counter} />
        <Input title="last name" disabled={!counter} />
      </Form.Row>
      <br />
      <Form.Row>
        <Input title="email" disabled={!counter} />
        <Input title="phone" disabled={!counter} />
      </Form.Row>
      <br />
      <Button variant="outline-info" type="submit" disabled={!counter} >Submit</Button>
    </Form>
  )
}

export default CartForm;

import React from 'react';
import { Col, Form } from 'react-bootstrap';
import { capitalizeFirstLetter } from '../../../utils/utils';

const Input = ({ title, disabled }) => {
  return (
    <Form.Group as={Col}>
      <Form.Control required type="text" placeholder={capitalizeFirstLetter(title)} disabled={disabled} id={title} />
      <Form.Control.Feedback type="invalid">Please provide a {title}</Form.Control.Feedback>
    </Form.Group>
  )
}

export default Input;

import React from 'react'
import { Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { decreaseCartProduct, increaseCartProduct } from '../../../redux/eCommerceSlice';
import style from "./TableRow.module.css";


const TableRow = ({ index, product }) => {
  const { code, title, price, quantity } = product;
  const dispatch = useDispatch();
  const plus = () => dispatch(increaseCartProduct(code));
  const minus = () => dispatch(decreaseCartProduct(code));

  return (
    <tr>
      <td>{index}</td>
      <td>{title}</td>
      <td className={style.codeTd}>{code}</td>
      <td className={style.buttonsCell}>
        <Button size="sm" variant="outline-info" onClick={minus}>-</Button>
        <Button size="sm" variant="outline-info" onClick={plus}>+</Button>
      </td>
      <td>{quantity}</td>
      <td className={style.priceTd}>${price}</td>
    </tr>
  )
}

export default TableRow;

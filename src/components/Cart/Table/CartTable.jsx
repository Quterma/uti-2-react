import React from 'react'
import { Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { selectCart, selectProducts } from '../../../redux/eCommerceSlice';
import { getCartTotal } from '../../../redux/storeGetters';
import TableRow from './TableRow';
// import { debounce } from "./../../../utils/debounce";
import style from "./CartTable.module.css";


const CartTable = () => {
  const storeProducts = useSelector(selectProducts);
  const storeCart = useSelector(selectCart);
  const total = getCartTotal(storeProducts, storeCart);

  // const [width, setWidth] = useState(window.innerWidth);
  // useEffect(() => {
  //   const debouncedHandleResize = debounce(() => setWidth(window.innerWidth), 1000);
  //   window.addEventListener('resize', debouncedHandleResize);
  //   return () => window.removeEventListener('resize', debouncedHandleResize);
  // });

  const mappedRow = Object.keys(storeCart)
    .map((code, i) => <TableRow key={code} index={i + 1} product={{ ...storeProducts[code], quantity: storeCart[code] }} />);

  return (
    <Table striped bordered hover variant="dark">
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th className={style.codeTh}>Code</th>
      <th></th>
      <th>Quantity</th>
      <th className={style.priceTh}>Price</th>
    </tr>
  </thead>
  <tbody>
    {mappedRow}
  </tbody>
  <tfoot>
    <tr>
      <th className={style.codeTh}></th>
      <th className={style.priceTh}></th>
      <th></th>
      <th></th>
      <th>Total</th>
      <th>${total}</th>
    </tr>
  </tfoot>
</Table>
  )
}

export default CartTable;

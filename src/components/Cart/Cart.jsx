import React from 'react';
import Container from 'react-bootstrap/Container';
import style from "./Cart.module.css";
import CartForm from './Form/CartForm';
import CartTable from './Table/CartTable';

const Cart = () => {
  return (
    <Container fluid="xl" className={style.grid}>
      <CartTable />
      <CartForm />
    </Container>
  )
}

export default Cart

import React from 'react';
import { Container, Navbar, Button, Badge } from 'react-bootstrap';
import { renderSwitch } from './../../utils/renderSwitch';
import { useDispatch, useSelector } from 'react-redux';
import { setPage, selectProducts, selectCart } from './../../redux/eCommerceSlice';
import { variables } from './../../utils/variables';
import style from "./Header.module.css";
import { getCartTotal, getCartCounter } from './../../redux/storeGetters';

const Header = ({ page }) => {
  const dispatch = useDispatch();
  const storeProducts = useSelector(selectProducts);
  const storeCart = useSelector(selectCart);

  const togglePage = () => dispatch(setPage(renderSwitch(page, variables.CART, variables.SHOWCASE)));

  const total = getCartTotal(storeProducts, storeCart);
  const counter = getCartCounter(storeCart);

  return (
    <Navbar bg="dark" variant="dark" fixed="top" className={style.navbar}>
      <Container fluid="xl">
        <Navbar.Brand className={style.flex1}>UTI-2-React</Navbar.Brand>
        {renderSwitch(page, <span className={style.total}><Badge variant="secondary">{total} $</Badge></span>, null)}
        <div className={style.relative}>
          {counter > 0 && renderSwitch(page, <span className={style.counter}><Badge variant="warning">{counter}</Badge></span>, null)}
          <Button variant="outline-info" onClick={togglePage}>{renderSwitch(page, "Cart", "Exit")}</Button>
        </div>
      </Container>
    </Navbar>
  )
}

export default Header;

import firebase from "firebase/app";
import "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyCsuLLacH9rNxUAGIWCzX12sGZUrYyda24",
	authDomain: "uti-shop.firebaseapp.com",
	projectId: "uti-shop",
	storageBucket: "uti-shop.appspot.com",
	messagingSenderId: "419591230927",
	appId: "1:419591230927:web:b2c203e138f97e9195aeba",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();
export default firebase;

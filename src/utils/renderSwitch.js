import { variables } from "./variables";

export const renderSwitch = (param, first, second) => {
	switch (param) {
		case variables.SHOWCASE:
			return first;
		case variables.CART:
			return second;
		default:
			return;
	}
};

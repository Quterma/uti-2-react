const isEmpty = obj => {
	if (!Object.keys(obj) || Object.keys(obj).length < 1) {
		return true;
	}
	return false;
};

export const getCartTotal = (storeProducts, storeCart) => {
	if (isEmpty(storeCart)) {
		return 0;
	}
	return Object.keys(storeCart).reduce((acc, curr) => acc + storeProducts[curr].price * storeCart[curr], 0);
};

export const getCartCounter = storeCart => {
	if (isEmpty(storeCart)) {
		return 0;
	}
	return Object.keys(storeCart).length;
};

export const getCartBody = (storeProducts, storeCart) => {
	if (isEmpty(storeCart)) {
		return 0;
	}
	return Object.keys(storeCart).reduce((acc, curr) => {
		const quantity = storeCart[curr];
		const { code, title, price } = storeProducts[curr];
		return { ...acc, [curr]: { code, title, price, quantity } };
	}, {});
};

import { createSlice } from "@reduxjs/toolkit";
// import uuid from "./../utils/uuid";
// import date from "./../utils/date";
import { getFirebaseProducts } from "./../api/api";

export const eCommerceSlice = createSlice({
	name: "eCommerce",
	initialState: {
		products: {
			// "code": { code, title, price, description }
		},
		cart: {
			// "code": quantity
		},
		// orders: {
		// id: {
		// 	user: { firstName, lastName, email, phone },
		// 	products: {
		// 		code: { code, title, price, quantity },
		// 	},
		// 	total,
		// },
		// },
		page: "SHOWCASE",
		modal: null,
	},
	reducers: {
		setProducts: (state, action) => {
			state.products = action.payload;
		},

		setCart: (state, action) => {
			state.cart = action.payload;
		},

		increaseCartProduct: (state, action) => {
			const code = action.payload;
			state.cart[code] ? (state.cart[code] += 1) : (state.cart[code] = 1);
		},

		decreaseCartProduct: (state, action) => {
			const code = action.payload;
			state.cart[code] > 1 ? (state.cart[code] -= 1) : delete state.cart[code];
		},

		// setOrder: (state, action) => {
		// 	const id = uuid();
		// 	const time = date();
		// 	state.orders[id] = { ...action.payload, time };
		// },

		setPage: (state, action) => {
			state.page = action.payload;
		},

		openModal: (state, action) => {
			state.modal = action.payload;
		},

		closeModal: state => {
			state.modal = null;
		},
	},
});

// reducers
export const {
	setProducts,
	setCart,
	increaseCartProduct,
	decreaseCartProduct,
	// setOrder,
	setPage,
	openModal,
	closeModal,
} = eCommerceSlice.actions;

// selectors
export const selectProducts = state => state.eCommerce.products;
export const selectCart = state => state.eCommerce.cart;
// export const selectOrders = state => state.eCommerce.orders;
export const selectPage = state => state.eCommerce.page;
export const selectModal = state => state.eCommerce.modal;

export const getProductsThunk = (db, collection) => async dispath => {
	const dbProducts = await getFirebaseProducts(db, collection);
	const newStoreProducts = dbProducts.reduce((acc, curr) => ({ ...acc, [curr.code]: curr }), {});
	dispath(setProducts(newStoreProducts));
};

export default eCommerceSlice.reducer;

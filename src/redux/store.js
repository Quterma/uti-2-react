import { configureStore } from "@reduxjs/toolkit";
import eCommerceReducer from "./eCommerceSlice";
import { throttle } from "./../utils/throttle";
import { loadState, saveState } from "./../utils/localStorage";

// get state from localStorage
const persistedState = loadState();

export const store = configureStore({
	reducer: {
		eCommerce: eCommerceReducer,
	},
	preloadedState: persistedState,
});

// subscribes for setting state into localStorage
store.subscribe(
	throttle(() => {
		saveState(store.getState());
	}),
	1000
);
